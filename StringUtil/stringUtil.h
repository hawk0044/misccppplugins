//
// Created by Александр Баканов on 20/02/2018.
//

#ifndef RECOGNIZECV_STRINGUTIL_H
#define RECOGNIZECV_STRINGUTIL_H
#include <string>
#include <vector>

using namespace std;
class stringUtil{
public:
    static string unescape(string &str);
    static string trim(string &str, string chars);
    static vector<string> split(string &str, char delim);
};

#endif //RECOGNIZECV_STRINGUTIL_H
