//
// Created by Александр Баканов on 20/02/2018.
//
#include "stringUtil.h"

using namespace std;
string stringUtil::unescape(string &str){
    string out;
    bool pending_escape = false;
    for(int i = 0; i < str.length()-1;i++){
        char v = str[i];
        if(v == '\\'){
            pending_escape = true;
            i++;
            continue;
        }

        if(!pending_escape){
            out += str[i];
            continue;
        }

        char vn = str[i+1];
        if(vn == 't'){
            out += '\t';
        }else if(vn=='n'){
            out += '\n';
        }else if(vn == '\\'){
            out += '\\';
        }else if(vn == '\"'){
            out += '\"';
        }else if(vn == '\''){
            out += '\'';
        }

        pending_escape = false;
    }

    return out;
}

string stringUtil::trim(string &str, string chars){
        string out = str;

        for(int i = 0;i<chars.length();i++){
            auto chr = chars[i];

            while(out[0] == chr){
                out = out.substr(1, out.length()-1);
            }

            while(out[out.length()-1] == chr){
                out = out.substr(0, out.length()-1);
            }
        }

        return out;
}

vector<string> stringUtil::split(string &str, char delim){
    vector<string> strings;

    size_t found = 0UL;
    size_t prev_found = string::npos;

    while((found = str.find(delim, found == 0 ? 0 : found+1)) != string::npos){
        size_t n = found - (prev_found == string::npos ? 0 : prev_found);
        if (n > str.length()) {
            cout << "Atten empty for " << str << " n = " << n << " length = " << str.length() << endl;
            strings.push_back("");
            break;
        }

        string sub = str.substr(prev_found == string::npos ? 0 : prev_found, n);

        strings.push_back(sub);
        prev_found = found;
    }

    if(prev_found != string::npos){
        if(prev_found == str.length()){
            strings.emplace_back("");
        }else{
            auto sub = str.substr(prev_found + 1, string::npos);
            strings.emplace_back(sub);
        }
    }

    return strings;
}
