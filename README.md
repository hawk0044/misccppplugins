
# **What is this repository for?** #
* Simple and usable C++ code pieces

## **WARNING** ##
### This code is not pretending on any optimizations or good alghorithms, it should just work and be simply-written.

### Feel free to pull-request your code

## What is available? ##

#### 1. Simple config reader (default values, file reloading, int-long-string cast types e.t.c) map<string,string> 
  * Load from file
  * Cast values to int, long, float, double, string
  * Store data as map<string,string>
  * Supports default values and auto-adding if they missing in file
  
#### 2. String utils for work with std::string
  * Unescape
  * Trim
  * Split by char
  
#### 3. Networking
  * UDP Client (tested on macOS, windows)

