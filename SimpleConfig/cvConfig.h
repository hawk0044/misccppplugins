//
// Created by Александр Баканов on 20/02/2018.
//

#ifndef RECOGNIZECV_CVCONFIG_H
#define RECOGNIZECV_CVCONFIG_H
#include <string>
#include <map>
#include <fstream>

#include "../StringUtil/stringUtil.h"

using namespace std;
class cvConfig {
public:
    string fileName;
    map<string, string> variables;

    cvConfig(string name, map<string,string> &default_map);
    string get_raw_string(string &key);
    string get_string(string key);
    long get_long(string key);
    int get_int(string key);
    double get_double(string key);
    float get_float(string key);
    void rewrite();
    void reload();

protected:
    bool fileExist();
    map<string,string> deserialize(string &data);
    string serialize();
};


#endif //RECOGNIZECV_CVCONFIG_H
