//
// Created by Александр Баканов on 20/02/2018.
//

#include "cvConfig.h"

cvConfig::cvConfig(string name, map<string,string> &default_map) :
        fileName(name),
        variables(default_map)
{
    reload();
}

string cvConfig::get_raw_string(string &key){
    return variables[key];
}

string cvConfig::get_string(string key){
    return stringUtil::unescape(variables[key]);
}

long cvConfig::get_long(string key){
    return atol(get_raw_string(key).c_str());
}

int cvConfig::get_int(string key){
    return (int)get_long(key);
}

double cvConfig::get_double(string key){
    return atof(get_raw_string(key).c_str());
}

float cvConfig::get_float(string key){
    return (float)get_double(key);
}

void cvConfig::rewrite(){
    auto str = serialize();

    auto file = fopen(fileName.c_str(), "w");
    if(file != nullptr){
        fwrite(str.c_str(), str.size(), 1, file);
        fclose(file);
    }
}

void cvConfig::reload(){
    if(!fileExist()){
        rewrite();
    }

    auto fp = fopen(fileName.c_str(), "r");
    if(fp != nullptr){
        fseek(fp, 0, SEEK_END);
        auto len = ftell(fp);
        rewind(fp);

        auto *bytes = (unsigned char*)malloc((size_t)len);
        fread(bytes, (size_t)len, 1, fp);
        fclose(fp);

        string str = string((char*)bytes);
        free(bytes);

        auto values = deserialize(str);
        for(auto i = values.begin(); i != values.end(); i++){
            variables[i->first] = i->second;
        }

        rewrite();
    }
}

bool cvConfig::fileExist(){
    return ifstream(fileName.c_str()).good();
}

map<string,string> cvConfig::deserialize(string &data){
    auto out = map<string,string>();
    size_t found = 0;
    auto prev_found = string::npos;

    while((found = data.find('\n', found + 1)) != string::npos){
        auto n = found - (prev_found == string::npos ? 0 : prev_found);
        string str = data.substr(prev_found == string::npos ? 0 : prev_found, n);

        str = stringUtil::trim(str, " \n\t\r");

        //Comment line
        if(str[0] == '#')
            continue;

        //Blank line
        if(str.length() <= 2)
            continue;

        auto values = stringUtil::split(str, '=');

        if(values.size() == 2){
            out[values[0]] = values[1];
        }

        prev_found = found;
    }

    return out;
}

string cvConfig::serialize(){
    string result;

    for(auto i = variables.begin(); i != variables.end(); i++){
        auto key = i->first;
        auto value = i->second;

        result += key;
        result += "=";
        result += value;
        result += "\n";
    }

    return result;
}
