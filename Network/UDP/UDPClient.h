//
// Created by Александр Баканов on 16/03/2018.
//

#ifndef RECOGNIZECV_UDPCLIENT_H
#define RECOGNIZECV_UDPCLIENT_H
#include <stdlib.h>
#include <string>
#include "NetworkException.h"

#ifdef _WIN32
    #include<winsock2.h>
    #pragma comment(lib,"ws2_32.lib") //Winsock Library
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <zconf.h>
#endif

using namespace std;
class UDPClient {
public:
    UDPClient(string _ip, int _port, int _buflen = 4096);
    virtual ~UDPClient();

    bool initialize();
    void uninitialize();
    int send(char *buf, size_t size);
protected:
#ifdef _WIN32
    struct sockaddr_in si_other;
    WSADATA wsa;
#else
    //struct hostent *server;
    //struct sockaddr_in serv_addr;
    //struct addrinfo *   f_addrinfo;
    struct sockaddr_in their_addr;
#endif

	int sockHandle;
    char *buffer;
    bool initialized;
    string ip;
    int port;
    int buflen;
};


#endif //RECOGNIZECV_UDPCLIENT_H
