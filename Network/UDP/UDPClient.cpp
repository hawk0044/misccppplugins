//
// Created by Александр Баканов on 16/03/2018.
//

#include "UDPClient.h"

UDPClient::UDPClient(string _ip, int _port, int _buflen) :
    ip(_ip),
    port(_port),
    buflen(_buflen),
    initialized(false),
    buffer(nullptr)
{
    buffer = new char[buflen];
}

UDPClient::~UDPClient()
{
    uninitialize();
}

#ifdef _WIN32
    bool UDPClient::initialize() {
        int slen=sizeof(si_other);

        if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
        {
            char excpt[512];
            sprintf(excpt, "Initialize failed %d", WSAGetLastError());
            throw new NetworkException(excpt);
        }

        if ( (this->sockHandle=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
        {
            char excpt[512];
            sprintf(excpt, "Socket create failed %d", WSAGetLastError());
            uninitialize();
            throw new NetworkException(excpt);
        }

        memset((char *) &si_other, 0, sizeof(si_other));
        si_other.sin_family = AF_INET;
        si_other.sin_port = htons(port);
        si_other.sin_addr.S_un.S_addr = inet_addr(ip.c_str());
        initialized = true;
        return true;
    }

    void UDPClient::uninitialize() {
        if(this->sockHandle != SOCKET_ERROR){
            closesocket(this->sockHandle);
        }

        WSACleanup();

        if(buffer != nullptr){
            delete [] buffer;
            buffer = nullptr;
        }

        initialized = false;
    }

	int UDPClient::send(char *buf, size_t size) {
		if (!initialized)
			return -4;

		if (buf == nullptr || size <= 0)
			return -2;

		if (this->sockHandle < 0)
			return -3;

		//printf("Send to %s:%s, %d bytes (%d socket)\n", ip.c_str(), decimal_port, (int)size, this->sockHandle);
		//auto ret = sendto(this->sockHandle, buf, (size_t)size, 0, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
		auto ret = sendto(this->sockHandle, buf, (size_t)size, 0, (struct sockaddr *)&si_other, sizeof(struct sockaddr));

		//freeaddrinfo(f_addrinfo);
		return (int)ret;
	}
#else
    bool UDPClient::initialize() {
        struct hostent *he = gethostbyname(ip.c_str());

        if((this->sockHandle = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
        {
            throw NetworkException("Unable to open socket");
        }

        their_addr.sin_family = AF_INET;
        their_addr.sin_port = htons(port);
        their_addr.sin_addr = *((struct in_addr *)he->h_addr);
        memset(&(their_addr.sin_zero), '\0', 8);

        this->initialized = true;
        return true;
    }

    void UDPClient::uninitialize() {
        if(sockHandle >= 0){
            close(sockHandle);
        }

        if(buffer != nullptr){
            delete [] buffer;
            buffer = nullptr;
        }

        initialized = false;
    }

    int UDPClient::send(char *buf, size_t size) {
        if(!initialized)
            return -4;

        if(buf == nullptr || size <= 0)
            return -2;

        if(this->sockHandle < 0)
            return -3;


        //printf("Send to %s:%s, %d bytes (%d socket)\n", ip.c_str(), decimal_port, (int)size, this->sockHandle);
        //auto ret = sendto(this->sockHandle, buf, (size_t)size, 0, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
        auto ret = sendto(this->sockHandle, buf, (size_t)size, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr));

        //freeaddrinfo(f_addrinfo);
        return (int)ret;
    }
#endif
